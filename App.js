
import './App.css';
import GetMethod from './Components/GetMethod';
import AxiosGet from './Components/AxiosGet';
import AxiosPost from './Components/AxiosPost';
import RhymsWord from './Components/RhymsWord';
import AsyncAwaitAxiosPost from './Components/AsyncAwaitAxiosPost';
import NavBar from './Components/NavBar';
import {Routes,Route} from 'react-router-dom'
import React from 'react';
// import Detail from './Components/Detail';
const Detail = React.lazy(()=>import('./Components/Detail'))



function App() {
  return (
   
    <div>
     {/* <GetMethod /> */}
     {/* <AxiosGet /> */}
     {/* <AxiosPost /> */}
     {/* <RhymsWord /> */}
     {/* <AsyncAwaitAxiosPost /> */}
     {/* <Suspense fallback={<div>Please wait data is loading ..........</div>}>
       <Detail />
     </Suspense> */}
     <Routes>
       <Route path="/" element={<NavBar />}></Route>
       <Route path="detail" element={
         <React.Suspense fallback='Loading..........'>
           <Detail />
         </React.Suspense>
       }></Route>
     </Routes>
     </div>
    
  );
}

export default App;
