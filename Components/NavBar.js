import React,{useState} from 'react';
import {useNavigate} from 'react-router-dom'


function NavBar() {
    const [data,setData] = useState("");
    const [answer,setAnswer] = useState([])
    const[slice,setSlice]=useState(5)
    //  const [filterdata,setFilterData] = useState([])
    // const [searchInput,setSearchInput] = useState("")

    const navigate = useNavigate();

   

    // const searchItems = (searchValues) => {
    //     setSearchInput(searchValues)
    //     if(searchInput!== '')
    //     {
    //         const filtered_data = answer.filter((item)=>{
    //             return Object.values(item).join(' ').toLowerCase().includes(searchInput.toLowerCase())
    //         })
    //         setFilterData(filtered_data)
    //     }
    //     else{
    //         setFilterData(answer)
    //     }

    // }

    const handleSearch = () => {
            fetch(`https://newsapi.org/v2/everything?q=${data}&apiKey=d3a68d3a93a54948a016a1553bc4d20c`)
            .then(response=>response.json())
            .then(data=>{
                setAnswer(data.articles)
                console.log(data.articles)
                setSlice(5)
            })
    }

   const clickhandler = () => {
    setSlice(prevalue=>prevalue+5)
   }


    const  handleDetail=(item)=>{
          console.log("hello")
          navigate("detail" , {state: {author: item.author, title: item.title, description: item.description}})
    }

  return (
    <div>
        
            <input type="text" placeholder='Search' onChange={(e)=>setData(e.target.value)} value={data} className="input_field" />
            <button type="submit" onClick={handleSearch} className="search_button">Search</button>

            <table border="2" className='table_content'>
                <thead>
                <tr>
                    <th>author</th>
                    <th>title</th>
                    <th>description</th> 
                </tr>
                </thead>
                <tbody> 
            {answer.slice(0,slice).map(item=>{
                return (
                <tr onClick={()=>handleDetail(item)}>
                    <td>{item.author}</td>
                    <td>{item.title}</td>
                    <td>{item.description}</td>
                </tr>
            )})}
            </tbody>  
            </table>

{ answer.length==0 || answer.length==slice ? null :<button onClick={clickhandler} className="loadmore">Load more</button>}

    </div>
  )
}

export default NavBar;