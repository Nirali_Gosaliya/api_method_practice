import React from 'react'
import {useLocation} from 'react-router-dom';

function Detail() {
    const location = useLocation();


  return (
    <div>
        <div>
          
            <p>{location.state.author}</p>
            <p>{location.state.title}</p>
            <p>{location.state.description}</p>

        </div>
    </div>
  )
}

export default Detail;