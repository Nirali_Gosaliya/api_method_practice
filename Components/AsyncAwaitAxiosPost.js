import React,{useEffect} from 'react'
import axios from 'axios'

function AsyncAwaitAxiosPost() {

    useEffect(()=>{
        const todo = async() => {
            const data = {
                title: "axios post method using function",
                body: "new example",
                userId: 1001
            }
           await axios.post('https://jsonplaceholder.typicode.com/posts' ,
            data)
            .then(response=>console.log(response.data))
            .catch((e)=>console.log(e))
        }
        todo();

        const perform =async () => {
            const userdata = {
                title: "Hello nirali",
                body: "i am persuing my training in react js",
                userId: 2001
            }
            await axios.post('https://jsonplaceholder.typicode.com/posts',
            userdata)
            .then(response=>console.log(response.data))
            .catch((e)=>console.log(e))
        }
        perform();
    },[])
  return (
   <>
    <div>POST REQUEST</div>
   </>
  )
}

export default AsyncAwaitAxiosPost;